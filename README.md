
incentive by [ElemeFE/style-guide](https://github.com/ElemeFE/style-guide)

### 编辑器配置
* 2 个空格缩进
* UTF-8 编码格式

## HTML
### 统一写法
* 所有标签和属性单词**小写**
* 属性值使用**双引号**进行包裹

### 可省略
* 在没有特殊需求的情况下**不省略**可选的结束标签
```html
<!-- 除了以下情况 -->
<ul>
  <li>1
  <li>2
</ul>
```
* 不省略可选的自结束标签末尾斜杠
* 自结束标签包含属性时在结束的斜杆前面应加**空格**
```html
<input type="text" />
```
* 所有 ```<a>``` 必须有 ```href``` 属性，如果**为空**时可以置为 ```href="JavaScript:"```

### 声明相关
* 使用 ```<!DOCTYPE html>``` 作为**唯一**的 DTD
* 使用 ```<meta charset="UTF-8" />``` 指定文件编码格式
* 所有页面必须有 ```<title>``` 标签，并尽可能地在不同页面使用不同的标题

### 结构相关
* 简化 HTML 结构，减少元素嵌套层数
* 严格遵守标签嵌套规则，禁止让标签出现在不正确的地方
```html
<!-- 禁止 -->
<dl>
  <dt>...</dt>
  <ul>
    <li>...</li>
  </ul>
</dl>

<!-- 允许 -->
<dl>
  <dt>...</dt>
  <dd>
    <ul>
      <li>...</li>
    </ul>
  </dd>
</dl>
```

### 其他
* 给图片添加 ```alt``` 属性
* 给无文字超连接添加 ```title``` 属性
* 给可视听的替换型元素内添加描述
```html
<audio>这是一个 audio 标签</audio>
```

## CSS
### 代码格式
* 左大括号与选择器之间留空，冒号后面留空，注释内外前后留空
```css
/* 注意前后空格 */
div { /* 注意前后空格 */ }
span {
  color: red; /* 注意前后空格 */
}
```
* 在只有一条样式时应当选择器写到同一行
```css
div { color: red; }
```
* 一个选择器中有多个样式声明时每条写一行
* 左大括号紧跟选择器，右大括号另起一行
* 多个选择器使用逗号隔开时写在不同的行，左大括号不要另起一行
```css
div,
p,
span {
  color: red;
  font-size: 12px;
}
```
* 每条样式声明后面都要加上分号
* 所有最外层引号使用双引号
* 用逗号分隔的多个样式值写成多行
```css
.block {
  box-shadow: 0 0 0 rgba(#000, 0.1),
              1px 1px 0 rgba(#000, 0.2),
              2px 2px 0 rgba(#000, 0.3),
              3px 3px 0 rgba(#000, 0.4),
              4px 4px 0 rgba(#000, 0.5);
}
```
### 注意
* 避免使用 ID 选择器
* 禁止使用 ```@import``` 引入 CSS 文件

## 其他
* 0 值的单位省略
* 16 进制颜色值中的字母统一小写
* 类名中只使用字母(一律小写)、数字以及“-”

## 模块化命名
* 类名使用完整英文单词或抽掉空格的英文词组
* 有且仅当有层级关系时使用“-”连接，比如组件内的元素类名采用组件名“-”子类名的形式：
```html
<div class="user">
  <div class="user-icon"></div>
  <div class="user-detail"></div>
</div>
```
在 CSS 中定义时应该平行写出来：
```css
.user {}
.user-icon {}
.user-detail {}
```
* 不要求类名的层级结构和 HTML 保持一致：
```html
<div class="grid">
  <div>
    <div class="grid-caption">
      <div class="grid-caption-text"></div>
      <div class="grid-caption-button"></div>
    </div>
  </div>
  <ul class="grid-row">
    <li class="grid-cell"></li>
  </ul>
</div>
```

* 允许模块穿插使用：
```html
<div class="header">
  <div class="container"> <!-- 穿插一个别的组件 -->
    <div class="header-logo"></div>
    <div class="header-nav"></div>
  </div>
</div>
```

* 不建议在高层级中放置低层级元素：
```html
<div class="module">
  <div class="module-caption">
    <div class="module-caption-content"> <!-- 推荐:低层嵌入高层 -->
      <div class="module-text"></div> <!-- 不建议:高层嵌如低层 -->
    </div>
    <!-- 允许:同级嵌套 -->
    <div class="module-content">
      <div class="module-text"></div>
    </div>
  </div>
</div>
```

* 尽可能地让样式可以用于更多标签，甚至可以任意调整结构：
```html
<style>
.article {}
.article-main {}
.article-title {}
</style>
<div class="article">
  <div class="article-main">
    <div class="article-title">
      <!-- ... -->
    </div>
      <!-- ... -->
  </div>
</div>
<div class="article">
  <div class="article-title">
    <!-- ... -->
  </div>
  <div class="article-main">
    <!-- ... -->
  </div>
</div>
```
* 一个组件可能有多种状态多种样式，可以在组件上添加修饰符来选择所需的样式：
```html
<!-- 在词法上推荐使用状态名词或形容词 -->
<div class="popup success">
  blah blah blah
</div>
<div class="popup warning">
  blah blah blah
</div>
<div class="popup error">
  blah blah blah
</div>
```
在选择器中使用多类来声明其样式：
```css
.popup.success {}
.popup.warning {}
.popup.error {}
```

## JavaScript  
### 基本
* 引号使用单引号
* 语句以分号结尾
* 左大括号不要另起一行

### 行的长度和换行
* 一行代码的长度不大于 80 个字符
* 当一行长度超过 80 个字符时，需要手动将一行拆成两行，新的一行增加两个层级的缩进
```JavaScript
// 示例
callAFunction(document, element, window, "some string value", true, 123, power,
        navigator);

// 逗号作为运算符，应在前一行的行尾，以下是不好的写法
callAFunction(document, element, window, "some string value", true, 123, power
        , navigator);
```

### 空白与空行
* 在二元和三元运算符的符号与操作数之间添加空格，在非行末的`,` `;` `}`后添加空格，在`{`前添加空格。并在每个逻辑块中间添加空白行：         
```JavaScript
var value = 'value';
var otherValue = value + 4;
var boole = true;

var test = function(one, two) {
  // ...
}

if (boole && arg) {
  // ...
} else if(boole) {
  // ...
} else if(!boole) {
  // ...
}
```

* 空行的使用包括下面这四种情况：
```JavaScript 
// 在方法之间
var hello = function() {
  // ...
}

var world = function() {
  // ...
}

// 在方法中的局部变量和第一条语句之间
var hello = function() {
  var num = 3;

  if(boole) {
    // ...
  };
};

// 在多行或单行注释之前（如本条）

// 在方法内的逻辑片段之间插入空格，提高可读性
if(boole) {
   
  for(i = 0; l = wl.length; i < l; i++) {
    // ...
  };
};
```

### 注释
* 使用`//`作为注释符，使用`/* */`作为多选注释符。注释符号与注释内容之间留空，注释的位置放在代码之上：
```JavaScript
/* 注释 */
// 注释
var comment = null;
```

* 未完成的代码中注释以`// TODO`并加上未完成的内容：
```JavaScript
if(true) {
  var dream = null;
  // TODO 这是一个未完成的代码
}
```
### 命名
* 变量的命名使用小写字母开头的驼峰命名法：
```JavaScript
var minNum = 0;
var maxRange = [0, 10];
```

* 常量的命名使用全大写字母：
```JavaScript
var URL = 'github.com';
```

### 定义
* 使用字面量
```JavaScript
// 不推荐
var str = new String('string');
var obj = new Object();
var arr = new Array();

// 推荐
var str = 'string';
var obj = {};
var arr = [];
```

* 变量有初始赋值使用单独的`var`：
```JavaScript
var MINNUM = 0;
var MAXNUM = 100;
var minNum, maxNum;
```
* 函数定义使用表达式来定义函数：
```JavaScript
// 不推荐
function hello() {
  // ...
};

// 推荐
var hello = function() {
  // ...
};
```

* 只引用一次的函数使用匿名定义：
```JavaScript
// 不推荐
var hello = function() {
  // ...
};
element.onclick = hello;

// 推荐
element.onclick = function() {
  // ...
};
```

* 自执行函数建议使用`void`前缀：
```JavaScript
void function() {
  // ...
}();
```

### 其他
* 比较运算符建议使用`=== / !==`

### 禁止事项
* 禁止使用 `eval`;
* 禁止使用 `with`;
* 禁止在块级作用域中使用函数声明语句
* 禁止使用 8 进制词法
* 禁止使用 `arguments` 映射
